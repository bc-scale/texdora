FROM quay.io/fedora/fedora:36

LABEL "org.opencontainers.image.source"="https://gitlab.com/benjaminBoboul/texdora.git" \
      "org.opencontainers.image.title"="TeXdora" \
      "org.opencontainers.image.base.name"="quay.io/fedora/fedora:36"

ENV PYTHONUNBUFFERED=1
ENV PATH=$PATH:/root/.local/bin
ENV TERM=xterm

RUN dnf install --nodocs --assumeyes \
        make \
        aspell \
        aspell-en \
        aspell-fr \
        texlive \
        texlive-luatex85 \
        texlive-plex-otf \
        texlive-menukeys \
        texlive-babel-english \
        texlive-babel-french \
        texlive-datetime2 \
        texlive-datetime2-english \
        texlive-datetime2-french \
        texlive-glossaries \
        texlive-glossaries-english \
        texlive-glossaries-french \
        texlive-hyphen-english \
        texlive-hyphen-french \
        texlive-minted \
        texlive-selnolig \
        texlive-glossaries-extra \
        texlive-pgfplots \
        texlive-thmtools \
        texlive-todonotes \
        texlive-siunitx \
        texlive-subfiles \
        texlive-circuitikz \
        texlive-xmpincl \
        texlive-pdfx \
        texlive-colorprofiles \
        texlive-moreenum \
        texlive-minitoc \
        texlive-appendix \
        texlive-lastpage \
        texlive-multirow \
        texlive-tcolorbox \
        texlive-titlesec \
        texlive-datetime \
        texlive-fontawesome5 \
        fontawesome-fonts \
        ibm-plex-mono-fonts \
        ibm-plex-sans-fonts \
        ibm-plex-serif-fonts \
        latexmk \
        python3 \
        python3-pip \
        pandoc \
        git \
        && dnf clean all \
        && ln -s $(which python3) /usr/bin/python \
        && fmtutil -sys --all \
        && luaotfload-tool --update

RUN python3 -m ensurepip \
    && python3 -m pip install --no-cache-dir --upgrade pip==22.2.2 \
    && python3 -m pip install --no-cache-dir --user pandoc-latex-environment==1.1.6 pandoc-include==1.2.0 pandoc-minted==0.2.0
